//
//  ReportViewController.swift
//  Illendula-TrackyourJunkFood
//
//  Created by student on 3/5/18.
//  Copyright © 2018 student. All rights reserved.
//

import UIKit

class ReportViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var reportLBL: UITextView!
    
    
    // MARK: - Navigation
     @IBAction func resetBTN(_ sender: UIButton) {
        AppDelegate.model.reset()
        reportLBL.text = AppDelegate.model.combinedReport()
     }
     /*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func unwindFromTable(segue:UIStoryboardSegue){
        reportLBL.text = AppDelegate.model.combinedReport()
    }

}
