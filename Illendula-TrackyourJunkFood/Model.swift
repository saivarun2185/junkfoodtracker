//
//  Model.swift
//  Illendula-TrackyourJunkFood
//
//  Created by student on 3/5/18.
//  Copyright © 2018 student. All rights reserved.
//

import Foundation

struct FoodData {
    
    
    var Name : String
    var Calories : Double
    var Tally : Int
    
}

class FoodRecorder {
    
    var Foods:[FoodData]
   var section:[String] = []
    
    init(){
        self.Foods = []
    }
    
    func loadData(){
        
        let Popcorn = FoodData(Name: "Popcorn", Calories: 106, Tally: 0)
        let Coke = FoodData(Name: "Coke", Calories: 140, Tally: 0)
        let PotatoChips = FoodData(Name: "PotatoChips", Calories: 152, Tally: 0)
        section.append(contentsOf: ["Snacks","Drinks","Fastfood"])
        self.Foods.append(contentsOf: [Popcorn,Coke,PotatoChips])
    }
    
    func increaseTally(forItem i:Int){
        self.Foods[i].Tally += 1
    }
    
    func getCalories(forItime i:Int) -> Int{
        return Int(self.Foods[i].Calories) * self.Foods[i].Tally
    }
    
    func report(forItem i:Int) -> String{
        return "The calories consumed for \( self.Foods[i].Name ) is  \(self.getCalories(forItime: i))"
    }
    
    func totalCalories() -> Int{
        var result = 0
        for i in 0...2{
            result = result + self.getCalories(forItime: i)
        }
        return result
    }
    
    func combinedReport() -> String{
        var itemReport = ""
        for i in 0..<3{
            itemReport = itemReport + self.report(forItem: i) + "\n"
        }
       itemReport = itemReport + "Total calories consumed \(self.totalCalories())"
        return itemReport
    }
    
    func reset(){
        for i in 0..<3 {
            self.Foods[i].Tally = 0
        }
    }
}
