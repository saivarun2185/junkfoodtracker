//
//  CustomTableViewCell.swift
//  Illendula-TrackyourJunkFood
//
//  Created by student on 3/7/18.
//  Copyright © 2018 student. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var leftLBL: UILabel!
    @IBOutlet weak var rightLBL: UILabel!
    
}
